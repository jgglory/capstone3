import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';


export default function ActiveProducts(){

	const [ products, setProducts ] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp = {product} />
				)
			}))
		})
	}, [])

	return (
		<>
			<h1>Products</h1>
			{products}
		</>
	)
}