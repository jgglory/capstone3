import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import { useParams } from 'react-router-dom';

export default function Product() {
	const { productId } = useParams();
	const [ product, setProduct ] = useState({});

	useEffect(() => {
	    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
	        method: "GET",
	        headers: {
	            "Content-Type": "application/json",
	            "Authorization": `Bearer ${localStorage.getItem('token')}`
	        }
	    })
	    .then(response => {
	        if (!response.ok) {
	            throw new Error(`HTTP error! Status: ${response.status}`);
	        }
	        return response.json();
	    })
	    .then(data => {
	        setProduct(data);
	    })
	    .catch(error => {
	        console.error(error);
	    });
	}, [productId]);

	return (
	    <div>
	        <ProductCard productProp={product} />
	    </div>
	);
}