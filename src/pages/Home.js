import { useEffect, useState } from 'react';
import data from '../data';
import { Link } from 'react-router-dom';

export default function Home() {

	const [ products, setProducts ] = useState([]);

	useEffect(()=> {
		const fetchData = async () => {
			const result = await fetch('api/products');
			setProducts(result.data);
		};
		fetchData()
	}, []);

	return (
		<div>
			<h1>Featured Products</h1>
			<div className="products">
			  {data.products.map(product => (
			    <div className="product" key={product.slug}>
			      <Link to={`/products/${product.slug}`}>
			      <img src={product.image} alt={product.name} />
			      </Link>
			      <div className="product-info">
			        <Link to={`/products/${product.slug}`}>
			          <p>{product.name}</p>
			        </Link>
			         <p><strong>${product.price}</strong></p>
			         <button>Add to cart</button>
			      </div>
			    </div>
			  ))}
			</div>	
		</div>
	);
}