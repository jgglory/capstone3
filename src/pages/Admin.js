import { useNavigate } from 'react-router-dom';
import { Button, Row, Col } from 'react-bootstrap';  

export default function AdminPage(){
  const navigate = useNavigate();

  const handleGetAllProducts = () => {
    navigate('/products');
  }

  const handleCreateProduct = () => {
    navigate('/createProduct');
  }


  return (
    <>
      <h1>Admin Dashboard</h1>
      <Row>
        <Col className="p-5 text-center">
          <Button className="m-5 p-3" onClick={handleGetAllProducts}>Get all Products</Button>
          <Button className="m-5 p-3" onClick={handleCreateProduct}>Create Product</Button>
        </Col>
      </Row>
    </>
  )
}