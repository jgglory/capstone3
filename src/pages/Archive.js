import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
//import { Navigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import ProductCard from '../components/ProductCard';


export default function Archive(){

  const {user} = useContext(UserContext);
  const { productId, name } = useParams();


  const toggleArchive = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({ isActive: false }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if(data){
          Swal.fire({
            title: "Archive Successful",
            icon: "success",
            text: `Product is now inactive`
          })
        }
      });
  };

  const toggleUnarchive = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({ isActive: true }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if(data){
          Swal.fire({
            title: "Unarchive Successful",
            icon: "success",
            text: `Product is now active`
          })
        }
      });
  };

  return (
    <>
    <Button className="m-2 p-2" onClick={toggleArchive}>Archive</Button>
    <Button className="m-2 p-2" onClick={toggleUnarchive}>Unarchive</Button>
    </>
  )  
}

