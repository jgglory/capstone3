import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
//import { Navigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Edit() {
	const {user} = useContext(UserContext);
	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);

	function editProduct(e) {
		e.preventDefault();

		// if (!user.isAdmin) {
		//   Swal.fire({
		//     title: "Authentication failed",
		//     icon: "error",
		//     text: "You are not an admin!"
		//   })
		//   return;
		// }

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/edit`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(!user.isAdmin) {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "You are not an admin!"
				})
			}else {
					Swal.fire({
						title: "Product updated",
						icon: "success",
						text: "Product has been successfully updated."
					})
			}

			setName('');
			setDescription('');
			setPrice('');
		})
	}

	useEffect(() => {
		if((name !== '' && description !== '' && price !== '')) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [productId, name, description, price])

	return (
		<Form onSubmit={(e) => editProduct(e)}>
			<Form.Group controlId = "productName">
				<Form.Label>Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter productName"
					required
					value={name}
					onChange={e => setName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId = "productDescription">
				<Form.Label>Description</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter productDescription"
					required
					value={description}
					onChange={e => setDescription(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId = "productPrice">
				<Form.Label>Price</Form.Label>
				<Form.Control
					type="number"
					placeholder="Enter productPrice"
					required
					value={price}
					onChange={e => setPrice(e.target.value)}
				/>
			</Form.Group>

			{isActive
				?
					<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
				:
					<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}

		</Form>
	)
}

/*export default function Edit(){
	const { productId } = useParams();

	const {user} = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price , setPrice] = useState(0);

	useEffect(() => {
		console.log(productId)

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[productId]);

	const editProduct = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/edit`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(!user.isAdmin) {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "You are not an admin!"
				})
			}else {
					Swal.fire({
						title: "Product updated",
						icon: "success",
						text: "Product has been successfully updated."
					})
			}
		})
	}

	return (
		<Form onSubmit={(e) => editProduct(e)}>
			<Form.Group controlId = "productName">
				<Form.Label>Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter productName"
					required
					value={name}
					onChange={e => setName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId = "productDescription">
				<Form.Label>Description</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter productDescription"
					required
					value={description}
					onChange={e => setDescription(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId = "productPrice">
				<Form.Label>Price</Form.Label>
				<Form.Control
					type="number"
					placeholder="Enter productPrice"
					required
					value={price}
					onChange={e => setPrice(e.target.value)}
				/>
			</Form.Group>

					<Button variant="primary" type="submit" id="submitBtn">Submit</Button>

		</Form>
	)
}*/

